# CommonCryptoSwift #

The goal of this project is to provide iOS CommonCrypto functionality to Swift frameworks delivered as such or via CocoaPods.

The idea would be that you're developing a Swift framework that you're distributing as a (private or not) CocoaPod; you would use this library through pod dependency (```pod CommonCryptoSwift```) and you'd be free to use any CommonCrypto functionality in your own framework.

Basically, the only thing this project does is it exposes "non-modular imports" of CommonCrypto as a module, using a ```modulemap```.

Currently, packaging as CocoaPod is not working - to be fixed.

### Author ###

* matti at 777-team dot org